import CourseCard from "../components/CourseCard"
import { useEffect, useState } from "react"

export default function Courses() {

	const [courses, setCourses] = useState([])

	useEffect(() => {
		fetch(`http://localhost:4000/courses/all`)
			.then(res => res.json())
			.then(data => {

				console.log(data)

				setCourses(data.map(course => {
					return (
							<CourseCard key={course._id} courseProps={course}/>
						)
				}))
			})
	}, [])



	return (
		<>
			<h1>Courses</h1>
			{courses}
		</>
	)
}
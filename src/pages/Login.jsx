import { Form, Button } from "react-bootstrap"
import { Navigate } from "react-router-dom"
import { useState, useEffect, useContext } from "react"
import UserContext from "../UserContext.jsx"
import Swal from 'sweetalert2'


export default function Login () {
	const { user, setUser } = useContext(UserContext)

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
    const [isActive, setIsActive] = useState(false)

	useEffect(() => {
    	if(email !== '' && password !== '') {
    		setIsActive(true)
    	} else {
    		setIsActive(false)
    	}	
    }, [email, password])

	function loginUser(e) {
    	e.preventDefault()

    	

    	fetch("http://localhost:4000/users/login", {
    		method: "POST",
    		headers: {
    			'Content-Type': 'application/json'
    		},
    		body: JSON.stringify({
    			email: email,
    			password: password
    		})
    	})
    	.then(res => res.json())
    	.then(data => {
    		console.log(data)
    		if(typeof data.access !== "undefined") {
    			localStorage.setItem('email', email)	
    			localStorage.setItem('token', data.access)
    			retrieveUserDetails(data.access)

    			Swal.fire({
    				title: "Login Successfull",
    				icon: "success",
    				text: "Welcome to Zuitt"
    			})
    		} else {
    			Swal.fire({
    				title: "Authentication Failed",
    				icon: "error",
    				text: "Check you login details and log in again"
    			})
    		}
    	})

    	setUser({
    		email: localStorage.getItem('email')
    	})

    	setEmail('')
    	setPassword('')
 	}	

 	const retrieveUserDetails = (token) => {
 		fetch('http://localhost:4000/users/details', {
 			headers: {
 				Authorization: `Bearer ${token}`
 			}
 		})
 		.then(res => res.json())
 		.then(data => {
 			console.log(data)

 			setUser({
 				id: data._id,
 				isAdmin: data.isAdmin
 			})
 		})
 	}


	return (
		(user.id !== null) ? (
			<Navigate to="/courses" />
		) : (
	        <Form onSubmit={loginUser}>
		      <Form.Group className="mb-3" controlId="userEmail">
	        	<h2>LOGIN</h2>

		        <Form.Label>Email address</Form.Label>
		        	<Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
		        <Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		        </Form.Text>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="password1">
		        <Form.Label>Password</Form.Label>
		        <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} required/>
		      </Form.Group>
	      	{ isActive ? (
		      		<Button variant="success" type="submit" controlId="submitBtn">
			        	LOGIN
			        </Button>
		        ) : (
		        	<Button variant="success" type="submit" controlId="submitBtn" disabled>
			        	LOGIN
			        </Button>
		        )
		  		}
		      
		    </Form>
	    )
    )
}
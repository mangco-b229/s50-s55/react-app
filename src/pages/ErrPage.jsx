import { Link } from "react-router-dom" 
import Banner from "../components/Banner"

export default function ErrPage () {
	const data = {
		title: "404 - Not Found",
		content: "the page you are looking  cannot be found",
		destination: "/",
		label: "Go back to homepage"
	}

	return (
		<div>
			<Banner data={data} />
		</div>
	)
}
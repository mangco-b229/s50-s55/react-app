import { Form, Button } from "react-bootstrap"
import { useState, useEffect, useContext } from "react"
import UserContext from "../UserContext.jsx"
import { useNavigate } from 'react-router-dom'
import Swal from "sweetalert2"




export default function Register() {
    const navigate = useNavigate()

    const [firstName, setFirstName] = useState("")
    const [lastName, setLastName] = useState("")
    const [mobileNum, setMobileNum] = useState("")
    const [email, setEmail] = useState("")
    const [password1, setPassword1] = useState("")
    const [password2, setPassword2] = useState("")
    const [isActive, setIsActive] = useState(false)

    const { user } = useContext(UserContext) 

    const register = () => {
      fetch("http://localhost:4000/users/register", {
        method: 'POST',
        body: JSON.stringify({
          email: email,
          password: password1,
          firstName: firstName,
          lastName: lastName,
          mobileNo: mobileNum
        }),
        headers: { "Content-Type": "application/json"}
      })
      .then(res => res.json())
      .then(data => {

        if(data === true) {
          Swal.fire({
            title: "Successfully Registered",
            icon: "success",
            text: "You have successfully registered!"              
          })
          setEmail("")
          setPassword1("")
          setPassword2("")
          navigate("/login")
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again"
          })
        }          
      })
    }

    const registerCheck = (e) => {
      e.preventDefault()

      fetch("http://localhost:4000/users/checkEmail", {
        method: "POST",
        body: JSON.stringify({ email }),
        headers: { "Content-Type": "application/json" }
      })
      .then(res => res.json())
      .then(data => {
        if(data === true) {
            Swal.fire({
              title: "email already exists",
              icon: "error",
              text: "Please use another email"
            })
        } else {
          register()
        }
      })

      
    }

    useEffect(() => {
      const allInputsFilled = email !== '' && password1 !== '' && password2 !== '' && firstName !== '' && lastName !== '' && mobileNum !== '';
      const passwordsMatch = password1 === password2;
      const mobileNumValid = mobileNum.length > 10;
      
      if (allInputsFilled && passwordsMatch && mobileNumValid) {
        setIsActive(true);
      } else {
        setIsActive(false);
      }
    }, [email, password1, password2, firstName, lastName, mobileNum])


    return (
    // <>
    //   {(user.email !== null) ? (
    //     <Navigate to="/courses" />
    //   ) : (
        <Form onSubmit={registerCheck}>
        <h2>REGISTER</h2>
          <Form.Group className="mb-3" controlId="userEmail">

            <Form.Label>Firstname</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter firstname"
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
              required
            />


            <Form.Label>Lastname</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter lastname"
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
              required
            />


            <Form.Label>Mobile Number</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter Moible number"
              value={mobileNum}
              onChange={(e) => setMobileNum(e.target.value)}
              required
            />
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />


            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>

          <Form.Group className="mb-3" controlId="password1">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              value={password1}
              onChange={(e) => setPassword1(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="password2">
            <Form.Label>Verify Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Verify Password"
              value={password2}
              onChange={(e) => setPassword2(e.target.value)}
              required
            />
          </Form.Group>

          {isActive ? (
            <Button variant="primary" type="submit" controlId="submitBtn" >
              REGISTER
            </Button>
          ) : (
            <Button variant="primary" type="submit" controlId="submitBtn"disabled>
              REGISTER
            </Button>
          )}
        </Form>
    //   )
    // }
    // </>
  )	
}
import AppNavBar from "./components/AppNavBar.jsx"
import CourseView from "./components/CourseView.jsx"
import { Container } from "react-bootstrap"
import { BrowserRouter as Router, Route, Routes } from "react-router-dom"
import { useState, useEffect } from "react"
import Courses from "./pages/Courses.jsx"
import Home from "./pages/Home.jsx"
import Register from "./pages/Register.jsx"
import Login from "./pages/Login.jsx"
import Logout from "./pages/Logout.jsx"
import ErrPage from "./pages/ErrPage.jsx"
import { UserProvider } from "./UserContext.jsx"

import './App.css'

function App() {
  const [user, setUser] = useState({
    //email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(() => {
    console.log(user)
    console.log(localStorage)
  })

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <div className="app">
        <Router>
          <AppNavBar />
          <Container>
            <Routes>
              <Route exact path="/" element={<Home />} />
              <Route exact path="/courses" element={<Courses />}/> 
              <Route exact path="/login" element={<Login />}/> 
              <Route exact path="/register" element={<Register />}/> 
              <Route exact path="/courseView/:courseId" element={<CourseView />}/> 
              <Route exact path="/logout" element={<Logout />}/> 
              <Route path="/*" element={<ErrPage />}/> 
            </Routes>
          </Container>
        </Router>      
      </div>
    </UserProvider>
  )
}

export default App

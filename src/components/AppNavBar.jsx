import { useState, useEffect } from "react"
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import { Link, NavLink } from "react-router-dom"
import Logout from "../pages/Logout"
import Login from "../pages/Login"
import { useContext } from "react"
import UserContext from "../UserContext.jsx"
 
export default function AppNavbar() {
	const { user } = useContext(UserContext)

	return (
		<Navbar bg="light" expand="lg" className="p-3">
			<Navbar.Brand href="#home">Zuitt</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">
					<Nav.Link as={NavLink} exact to="/">Home</Nav.Link>
					<Nav.Link as={NavLink}  exact to="/courses">Courses</Nav.Link>
					{ 					
						(user.id !== null) ? 
						<Nav.Link as={NavLink} exact to="/logout">Logout</Nav.Link>
						:
						<>
							<Nav.Link as={NavLink} exact to="/login">Login</Nav.Link>
							<Nav.Link as={NavLink} exact to="/register">Register</Nav.Link>
						</>					
					}
									
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)
}
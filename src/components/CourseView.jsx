// import Courses from "./pages/Courses"
import { useState, useEffect, useContext } from "react"
import { Container, Card, Button, Col, Row } from "react-bootstrap"
import { useParams, useNavigate, Link } from "react-router-dom"
import UserContext from "../UserContext.jsx"
import Swal from "sweetalert2"
 
export default function CourseView () {
	const navigate = useNavigate()

	const { user } = useContext(UserContext)
	const { courseId } = useParams()

	const [name ,setName] = useState("")
	const [description, setDescription] = useState("")
	const [price , setPrice] = useState(0)

	useEffect(() => {
		console.log(courseId)

		fetch(`http://localhost:4000/courses/${courseId}`)
			.then(res => res.json())
			.then(data => {
				console.log(data)
				setName(data.name)
				setDescription(data.description)
				setPrice(data.price)
			})
	},[])

	const enroll = (courseId) => {
		fetch("http://localhost:4000/users/enroll", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);
			if(data === true) {
				Swal.fire({
					title: "Successfully Enrolled",
					icon: "success",
					text: "You have successfully enrolled for this course!"
				})
				navigate("/courses")
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}
		});
	}

	return
	(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							<Card.Subtitle>Class Schedule</Card.Subtitle>
							<Card.Text>8 am - 5 pm</Card.Text>
							{
								(user.id !== null) ? 
									<Button variant="primary" onClick={() => enroll(courseId)}>Enroll</Button>
								:
									<Link className="btn btn-danger btn-black" to="/login">Login</Link> 
							}
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	)
}

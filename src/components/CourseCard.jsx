import { Card, Button } from "react-bootstrap"
import { useState } from "react"
import {Link} from "react-router-dom" 


export default function CourseCard({courseProps}) {
	const {name, description, price, _id} = courseProps 

	const [seats, setSeats] = useState(30)
	const [count, setCount] = useState(0)

	return (
		<div className="my-4">
			<Card className="cardHighlight p-3">
				<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Subtitle>Description</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>{price}</Card.Text>
					<Card.Text>Enrollees: {count}</Card.Text>
					<Link className="btn btn-primary" to={`/courseView/${_id}`} block>DETAILS</Link>
				</Card.Body>
			</Card>
		</div>
	)
}
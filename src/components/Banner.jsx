import { Row, Col, Button } from "react-bootstrap"
import { Link } from "react-router-dom" 

export default function Banner({ data }) {

	const { title, content, destination, label } = data
	return (
		<Row className="p-5 text-center">
			<Col>
				<h1>{title}</h1>
				<p>{content}</p>
				<Button variant="primary" as={Link} to={destination}>{label}</Button>
			</Col>
		</Row>
	)
}